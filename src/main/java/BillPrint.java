import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * BillPrint - adapted from Refactoring, 2nd Edition by Martin Fowler
 *
 * @author Minglun Zhang
 *
 */
public class BillPrint {
	private HashMap<String, Play> plays;
	private String customer;
	private ArrayList<Performance> performances;
	private int totalAmount;
	private int totalVolumeCredits;

	public BillPrint(ArrayList<Play> plays, String customer, ArrayList<Performance> performances) {
		this.plays = new HashMap();
		for (Play p : plays) {
			this.plays.put(p.getId(), p);
		}

		this.customer = customer;
		this.performances = performances;
		this.totalAmount =totalAmount();
		this.totalVolumeCredits =totalVolumeCredits();
	}

	public HashMap<String, Play> getPlays() {
		return plays;
	}


	public String getCustomer() {
		return customer;
	}

	public ArrayList<Performance> getPerformances() {
		return performances;
	}



	Play playFor (Performance aPerformance){

			return plays.get(aPerformance.getPlayID());}
	public int amountFor (Performance aPerformance) {
		int result = 0;
		switch (playFor(aPerformance).getType()) {
			case "tragedy":
				result = 40000;
				if (aPerformance.getAudience() > 30) {
					result += 1000 * (aPerformance.getAudience() - 30);
				}
				break;
			case "comedy":
				result = 30000;
				if (aPerformance.getAudience() > 20) {
					result += 10000 + 500 * (aPerformance.getAudience() - 20);
				}
				result += 300 * aPerformance.getAudience();
				break;
			default:
				throw new IllegalArgumentException("unknown type: " + playFor(aPerformance));
		}
		return result;
	}
	public int volumeCreditsFor (Performance perf){
			int result = 0;
			result += Math.max(perf.getAudience() - 30, 0);
			if ("comedy" == playFor(perf).getType()) result += Math.floor(perf.getAudience() / 5);
			return result;}
	public String usd(double aNumber) {
		DecimalFormat numberFormat = new DecimalFormat("#.00");
		return "$" + numberFormat.format((double) aNumber / 100.00);
	}

	int totalVolumeCredits () {
			int result = 0;
			for (Performance perf : performances) {
				result += volumeCreditsFor(perf);
			}
			return result;}
	int totalAmount () {
			int result = 0;
			for (Performance perf : performances) {
				result += amountFor(perf);
			}
			return result;
		}
	public String statement(){
		return renderPlainText();
	}

	String renderPlainText() {
		DecimalFormat numberFormat = new DecimalFormat("#.00");

			String result = "Statement for " + this.customer + "\n";
			for (Performance perf: performances) {

				if (playFor(perf) == null) {
					throw new IllegalArgumentException("No play found");
				}

				// print line for this order
				result += "  " + playFor(perf).getName() + ": " + usd( amountFor(perf)) + " (" + perf.getAudience()
						+ " seats)" + "\n";
			}

			result += "Amount owed is " + usd( totalAmount()) + "\n";
			result += "You earned " + totalVolumeCredits() + " credits" + "\n";
			return result;

		}

	public static void main(String[] args) {
		Play p1 = new Play("hamlet", "Hamlet", "tragedy");
		Play p2 = new Play("as-like", "As You Like It", "comedy");
		Play p3 = new Play("othello", "Othello", "tragedy");
		ArrayList<Play> pList = new ArrayList<Play>();
		pList.add(p1);
		pList.add(p2);
		pList.add(p3);
		Performance per1 = new Performance("hamlet", 55);
		Performance per2 = new Performance("as-like", 35);
		Performance per3 = new Performance("othello", 40);
		ArrayList<Performance> perList = new ArrayList<Performance>();
		perList.add(per1);
		perList.add(per2);
		perList.add(per3);
		String customer = "BigCo";
		BillPrint app = new BillPrint(pList, customer, perList);
		System.out.println(app.statement());
	}

}
